<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Peminjaman_model extends CI_Model 
    {
        function create($data) 
        {
            $this->db->insert("peminjaman", $data);
        }

        function readJoinsarpra($where = "" , $order = "")
        {
            if(!empty($where)) $this->db->where($where);
            if(!empty($where)) $this->db->order_by($order);

            $query = $this->db->select("
                sarana_prasarana.id as id,
                peminjaman.id as id_peminjaman,
                peminjaman.id_user as id_user,
                sarana_prasarana.nama_sarana_prasarana as nama_sarana_prasarana,
                peminjaman.tanggal_pengajuan as tanggal_pengajuan,
                peminjaman.tanggal_peminjaman as tanggal_peminjaman,
                peminjaman.tanggal_pengembalian as tanggal_pengembalian,
                peminjaman.jumlah as jumlah,
                peminjaman.keperluan_peminjaman as keperluan_peminjaman,
                peminjaman.status as status,
                user.nama_lengkap as nama_lengkap,
                peminjaman.keterangan as keterangan,
            ");

            $this->db->join("user", "user.id = peminjaman.id_user");
            $this->db->join("sarana_prasarana", "sarana_prasarana.id = peminjaman.id_sarana_prasarana");

            $query = $this->db->get("peminjaman");

            if($query AND $query->num_rows() != 0) {
                return $query->result();
            } else {
                return array();
            }
        }

        function read($where = "", $order = "") 
        {
            if(!empty($where)) $this->db->where($where);
            if(!empty($where)) $this->db->order_by($order);

            $query = $this->db->get("peminjaman");

            if($query AND $query->num_rows() != 0) {
                return $query->result();
            } else {
                return array();
            }
        }

        function update($id, $data)
        {
            $this->db->where($id);
            $this->db->update("peminjaman", $data);
        }

        function delete($id)
        {
            $this->db->where($id);
            $this->db->delete("peminjaman");
        }
    }
?>