<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Monitoring_model extends CI_Model 
    {
        function create($data) 
        {
            $this->db->insert("monitoring", $data);
        }

        function read($where = "", $order = "") 
        {
            if(!empty($where)) $this->db->where($where);
            if(!empty($where)) $this->db->order_by($order);

            $query = $this->db->get("monitoring");

            if($query AND $query->num_rows() != 0) {
                return $query->result();
            } else {
                return array();
            }
        }

        function update($id, $data)
        {
            $this->db->where($id);
            $this->db->update("monitoring", $data);
        }

        function delete($id)
        {
            $this->db->where($id);
            $this->db->delete("monitoring");
        }
    }
?>