<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Sarpra_model extends CI_Model 
    {
        function create($data) 
        {
            $this->db->insert("sarana_prasarana", $data);
        }

        function read($where = "", $order = "") 
        {
            if(!empty($where)) $this->db->where($where);
            if(!empty($order)) $this->db->order_by($order);

            $query = $this->db->get("sarana_prasarana");

            if($query AND $query->num_rows() != 0) {
                return $query->result();
            } else {
                return array();
            }
        }

        function update($id, $data)
        {
            $this->db->where($id);
            $this->db->update("sarana_prasarana", $data);
        }

        function delete($id)
        {
            $this->db->where($id);
            $this->db->delete("sarana_prasarana");
        }
    }
?>