
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= base_url('assets')?>/dist/img/people.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?= $this->session->userdata("username") ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="<?=  $this->uri->segment(1) == 'dashboard' ? 'active': '' ?>"><a href="<?= site_url('dashboard'); ?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>        
            <?php
                if($this->session->userdata("id_level") == 1) {
                    ?>
                        <li class="<?=  $this->uri->segment(1) == 'sarpra' ? 'active': '' ?>"><a href="<?= site_url('sarpra'); ?>"><i class="fa fa-binoculars"></i> <span>Sarana Prasarana</span></a></li>       
                    <?php
                }
            ?>
            <li class="treeview <?= $this->uri->segment(1) == 'peminjaman' ? 'active': ''?>">
                <a href="#">
                    <i class="fa fa-address-card"></i> <span>Peminjaman</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <?php
                        if($this->session->userdata("id_level") != 1) {
                            ?>
                                <li class=""><a href="<?= site_url('peminjaman')?>"><i class="fa fa-circle-o"></i> Pinjam Sarpra</a></li>
                            <?php
                        }
                    ?>
                    <li class=""><a href="<?= site_url('peminjaman/histori')?>"><i class="fa fa-circle-o"></i>  History Peminjaman</a></li>
                </ul>
            </li>
            <?php
                if($this->session->userdata("id_level") == 1) {
                    ?>
                        <li class="<?=  $this->uri->segment(1) == 'laporan' ? 'active': '' ?>"><a href="<?= site_url('laporan'); ?>"><i class="fa fa-book"></i> <span>Daftar Laporan</span></a></li>        
                        <li class="<?=  $this->uri->segment(1) == 'maintenance' ? 'active': '' ?>"><a href="<?= site_url('maintenance'); ?>"><i class="fa fa-gears"></i> <span>Maintenance</span></a></li>  
                    <?php
                }
            ?>      
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>