<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
      <h1>
          Sarana Prasarana
          <small>Tambah Data</small>
      </h1>
  </section>

  <!-- Main content -->
  <section class="content">
        <div class="row">
            <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-danger">
                        <div class="box-header with-border">
                            <a href="<?= site_url('sarpra') ?>" class="btn btn-success"><i class="fa fa-chevron-left"></i> Kembali</a>
                        </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                        <form role="form" method="POST" action="<?= site_url('sarpra/store')?>">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Nama Sarana Prasarana</label>
                                    <input type="text" class="form-control" name="nama_sarana_prasarana" placeholder="Nama Sarana Prasarana" required>
                                </div>

                                <div class="form-group">
                                    <label>Kode Inventaris</label>
                                    <input type="text" class="form-control" name="kode_inventaris" placeholder="Kode Inventaris" required>
                                </div>

                                <div class="form-group">
                                    <label>Usia Ekonomis (Tahun)</label>
                                    <input type="number" class="form-control" name="usia_ekonomis" placeholder="Usia Ekonomis" required>
                                </div>

                                <div class="form-group">
                                    <label>Stok</label>
                                    <input type="number" class="form-control" name="stok" placeholder="Stok" required>
                                </div>

                                <div class="form-group">
                                    <label for="status">Status</label>
                                    <select name="status" id="" class="form-control" required>
                                        <option value="">[ Pilih Status ]</option>
                                        <option value="1">Tersedia</option>
                                        <option value="2">Tidak Tersedia</option>
                                        <option value="3">Maintenance</option>
                                        <option value="4">Rusak</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Tanggal Masuk</label>
                                    <input type="date" class="form-control" name="tanggal_masuk" placeholder="Tanggal Masuk" required>
                                </div>

                                <div class="form-group">
                                    <label>Keterangan</label>
                                    <textarea name="keterangan" id="" cols="30" rows="10" class="form-control" placeholder="Keterangan" required></textarea>
                                </div>
                            </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            <!-- /.box -->
            </div>
            <!--/.col (left) -->
        </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
  <!-- /.content-wrapper -->