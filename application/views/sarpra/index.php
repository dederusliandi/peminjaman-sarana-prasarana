<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Sarana Prasarana
            <small>data</small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <a href="<?= site_url('sarpra/create') ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>
                    </div>  
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                  <th>No</th>
                                  <th>Kode Inventaris</th>
                                  <th>Nama Sarana Prasarana</th>
                                  <th>Usia (Tahun)</th>
                                  <th>Stok</th>
                                  <th>Status</th>
                                  <th>Tanggal masuk</th>
                                  <th>Keterangan</th>
                                  <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    foreach($result as $index => $data) {
                                        ?>
                                            <tr>
                                                <td><?= $index + 1 ?></td>
                                                <td><?= $data->kode_inventaris ?></td>
                                                <td><?= $data->nama_sarana_prasarana ?></td>
                                                <td><?= $data->usia_ekonomis ?></td>
                                                <td><?= $data->stok ?></td>
                                                <td>
                                                    <?php
                                                        if($data->status == 1) {
                                                            echo "<label class='label label-success'>tersedia</label>";
                                                        } elseif($data->status == 2) {
                                                            echo "<label class='label label-default'>tidak tersedia</label>";
                                                        } else if($data->status == 3) {
                                                            echo "<label class='label label-warning'>maintenance</label>";                                                            
                                                        } else {
                                                            echo "<label class='label label-danger'>rusak</label>";
                                                        }
                                                        
                                                    ?>
                                                </td>
                                                <td><?= date('d-m-Y',strtotime($data->tanggal_masuk)) ?></td>
                                                <td><?= $data->keterangan ?></td>
                                                <td>
                                                    <a href="<?= site_url('sarpra/edit/'.$data->id) ?>" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i> Edit</a>
                                                </td>
                                            </tr>
                                        <?php
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->