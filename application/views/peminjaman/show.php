<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Detail Peminjaman
            <small>detail pinjaman</small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <a href="<?= site_url('peminjaman/histori') ?>" class="btn btn-success"><i class="fa fa-v"></i> Kembali</a>
                    </div>  
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="" class="table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <td width="250px">Nama Sarana Prasarana</td>
                                    <td width="20px">:</td>
                                    <td><?= $result->nama_sarana_prasarana ?></td>
                                </tr>
                                <tr>
                                    <td width="250px">Tanggal Pengajuan</td>
                                    <td width="20px">:</td>
                                    <td><?= date('d-m-Y',strtotime($result->tanggal_pengajuan)) ?></td>
                                </tr>
                                <tr>
                                    <td width="250px">Tanggal Peminjaman</td>
                                    <td width="20px">:</td>
                                    <td><?= date('d-m-Y',strtotime($result->tanggal_peminjaman)) ?></td>
                                </tr>
                                <tr>
                                    <td width="250px">Tanggal Pengembalian</td>
                                    <td width="20px">:</td>
                                    <td><?= date('d-m-Y',strtotime($result->tanggal_pengembalian)) ?></td>                                    
                                </tr>
                                <tr>
                                    <td width="250px">Jumlah Peminjaman</td>
                                    <td width="20px">:</td>
                                    <td><?= $result->jumlah ?></td>
                                </tr>
                                <tr>
                                    <td width="250px">Keperluan Peminjaman</td>
                                    <td width="20px">:</td>
                                    <td><?= $result->keperluan_peminjaman ?></td>
                                </tr>
                                <tr>
                                    <td width="250px">Keperluan Peminjaman</td>
                                    <td width="20px">:</td>
                                    <td>
                                        <?php
                                            if($result->status == 1)  {
                                                echo "<label class='label label-warning'>Belum di ACC</label>";
                                            } elseif($result->status == 2) {
                                                echo "<label class='label label-success'>Acc</label>";                                                            
                                            } elseif($result->status == 3) {
                                                echo "<label class='label label-danger'>tidak di ACC</label>";                                                            
                                            } else {
                                                echo "<label class='label label-primary'>dikembalikan</label>";                                                            
                                            }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="250px">Keterangan</td>
                                    <td width="20px">:</td>
                                    <td><?= $result->keterangan ?></td>
                                </tr>
                               
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->