<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Histori Peminjaman
            <small>data</small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        
                    </div>  
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped" style="font-size:9pt;">
                            <thead>
                                <tr>
                                  <th>No</th>
                                  <?php
                                    if($this->session->userdata("id_level") == 1) {
                                        echo "<th>Peminjam</th>";
                                    }
                                ?>
                                  <th>Nama Sarana Prasarana</th>
                                  <th>Tanggal Pengajuan</th>
                                  <th>Tanggal Peminjaman</th>
                                  <th>Tanggal Pengembalian</th>
                                  <th>Jumlah</th>
                                  <th>Keperluan</th>
                                  <th>Status</th>
                                  <?php
                                    if($this->session->userdata("id_level") == 1) {
                                        ?>
                                            <th>Aksi</th>
                                        <?php
                                    }
                                  ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    foreach($dataHistori as $index => $histori) {
                                        ?>
                                            <tr>
                                               <td><?= $index + 1 ?></td>
                                               <?php
                                                    if($this->session->userdata("id_level") == 1) {
                                                        echo "<td>".$histori->nama_lengkap."</td>";
                                                    }
                                                ?>
                                               <td><a href="<?= site_url('peminjaman/show/'.$histori->id_peminjaman) ?>"><?= $histori->nama_sarana_prasarana ?></a></td>
                                               <td><?= date('d-m-Y',strtotime($histori->tanggal_pengajuan)) ?></td>
                                               <td><?= date('d-m-Y', strtotime($histori->tanggal_peminjaman)) ?></td>
                                               <td><?= date('d-m-Y', strtotime($histori->tanggal_pengembalian)) ?></td>
                                               <td><?= $histori->jumlah ?></td>
                                               <td><?= $histori->keperluan_peminjaman ?></td>
                                               <td>
                                                   <?php
                                                        if($histori->status == 1)  {
                                                            echo "<label class='label label-warning'>Belum di ACC</label>";
                                                        } elseif($histori->status == 2) {
                                                            echo "<label class='label label-success'>Acc</label>";                                                            
                                                        } elseif($histori->status == 3) {
                                                            echo "<label class='label label-danger'>tidak di ACC</label>";                                                            
                                                        } else {
                                                            echo "<label class='label label-primary'>dikembalikan</label>";                                                            
                                                        }
                                                   ?>
                                               </td>
                                               <?php
                                                if($this->session->userdata("id_level") == 1) {
                                                    ?>
                                                        <td>
                                                            <?php
                                                                 if($histori->status == 1) {
                                                                    ?>
                                                                        <a href="<?= site_url('peminjaman/acc/'.$histori->id_peminjaman) ?>" class="btn btn-success btn-sm">Acc</a>
                                                                        <a href="<?= site_url('peminjaman/tolak/'.$histori->id_peminjaman) ?>" class="btn btn-danger btn-sm">Tolak</a>
                                                                    <?php
                                                                } elseif($histori->status == 2) {
                                                                    ?>
                                                                        <a href="<?= site_url('peminjaman/selesai/'.$histori->id_peminjaman) ?>" class="btn btn-success btn-sm">selesai</a>
                                                                    <?php
                                                                } else {
                                                                    echo "-";
                                                                }
                                                            ?>
                                                        </td>
                                                    <?php
                                                }
                                               ?>
                                            </tr>
                                        <?php
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->