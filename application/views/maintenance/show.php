<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Detail Maintenance
            <small>data</small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <a href="<?= site_url('maintenance') ?>" class="btn btn-success"><i class="fa fa-chevron-left"></i> Kembali</a>
                    </div>  
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                  <th>No</th>
                                  <th>Tanggal Monitoring</th>
                                  <th>Status</th>
                                  <th>Keterangan</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    foreach($result as $index => $data) {
                                        ?>
                                            <tr>
                                                <td><?= $index + 1 ?></td>
                                                <td><?= date('d-m-Y',strtotime($data->tanggal_maintenance)) ?></td>
                                                <td>
                                                    <?php
                                                        if($data->status == 1) {
                                                            echo "baik";
                                                        } else if($data->status == 2) {
                                                            echo "rusak";
                                                        } else {
                                                            echo "perbaikan";
                                                        }
                                                    ?>
                                                    </td>
                                                <td><?= $data->keterangan ?></td>
                                            </tr>
                                        <?php
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->