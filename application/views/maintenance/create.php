<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
      <h1>
          Maintenance
          <small>Tambah Data</small>
      </h1>
  </section>

  <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-danger">
                    <div class="box-header">
                        <a href="<?= site_url('peminjaman') ?>" class="btn btn-success"><i class="fa fa-chevron-left"></i> Kembali</a>                        
                    </div>  
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="" class="table table-bordered table-striped">
                           <tbody>
                               <tr>
                                   <th width="200px">Kode Inventaris</th>
                                   <td width="50px">:</td>
                                   <td><?= $sarpra->kode_inventaris ?></td>
                               </tr>
                               <tr>
                                   <th>Nama Sarana Prasarana</th>
                                   <td>:</td>
                                   <td><?= $sarpra->nama_sarana_prasarana ?></td>
                               </tr>
                               <tr>
                                   <th>Usia Ekonomis</th>
                                   <td>:</td>
                                   <td><?= $sarpra->usia_ekonomis ?></td>
                               </tr>
                               <tr>
                                   <th>Stok</th>
                                   <td>:</td>
                                   <td><?= $sarpra->stok ?></td>
                               </tr>
                               <tr>
                                   <th>Status</th>
                                   <td>:</td>
                                   <td>
                                       <?php
                                            if($sarpra->status == 1) {
                                                echo "<label class='label label-success'>tersedia</label>";
                                            } elseif($sarpra->status == 2) {
                                                echo "<label class='label label-default'>tidak tersedia</label>";
                                            } else if($sarpra->status == 3) {
                                                echo "<label class='label label-warning'>maintenance</label>";                                                            
                                            } else {
                                                echo "<label class='label label-danger'>rusak</label>";
                                            }
                                       ?>
                                   </td>
                               </tr>
                               <tr>
                                   <th>Tanggal Masuk</th>
                                   <td>:</td>
                                   <td><?= $sarpra->tanggal_masuk ?></td>
                               </tr>
                               <tr>
                                   <th>Keterangan</th>
                                   <td>:</td>
                                   <td><?= $sarpra->keterangan ?></td>
                               </tr>
                           </tbody>
                         
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
          <!-- /.col -->
        </div>
        <div class="row">
            <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-danger">
                        <div class="box-header with-border">
                        </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                        <form role="form" method="POST" action="<?= site_url('maintenance/'.$sarpra->id.'/store')?>" id="form_peminjaman">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Tanggal Monitoring</label>
                                    <input type="date" class="form-control" name="tanggal_maintenance" placeholder="Tanggal Monitoring" required>
                                </div>

                                <div class="form-group">
                                    <label>Jumlah Maintenance</label>
                                    <input type="number" class="form-control" name="total_maintenance" placeholder="Total Maintenance" required>
                                </div>

                                <div class="form-group">
                                    <label>Status</label>
                                    <select name="status" id="" class="form-control">
                                        <option value="">[ Pilih Status ]</option>
                                        <option value="1">Baik</option>
                                        <option value="2">Rusak</option>
                                        <option value="3">Perbaikan</option>
                                    </select>
                                </div>
                                
                                <div class="form-group">
                                    <label>Keterangan</label>
                                    <textarea name="keterangan" id="" cols="30" rows="10" class="form-control" placeholder="Keterangan" required></textarea>
                                </div>
                            </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary" id="button_peminjaman">Submit</button>
                        </div>
                    </form>
                </div>
            <!-- /.box -->
            </div>
            <!--/.col (left) -->
        </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
  <!-- /.content-wrapper -->