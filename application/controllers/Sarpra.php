<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sarpra extends CI_COntroller
{
    function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');

        if(!$this->session->userdata("username")) redirect("login");

        $this->load->model('Sarpra_model');
    }

    function index()
    {
        $data['result'] = $this->Sarpra_model->read("","id DESC");
        $data['view'] = "sarpra/index";
        $this->load->view('index', $data);
    }

    function create()
    {
        $data['view'] = "sarpra/create";
        $this->load->view('index', $data);
    }

    function store()
    {
        $dataStore = array(
            'kode_inventaris' => $this->input->post('kode_inventaris'),
            'nama_sarana_prasarana' => $this->input->post('nama_sarana_prasarana'),
            'usia_ekonomis' => $this->input->post('usia_ekonomis'),
            'status' => $this->input->post('status'),
            'stok' => $this->input->post('stok'),
            'tanggal_masuk' => $this->input->post('tanggal_masuk'),
            'keterangan' => $this->input->post('keterangan'),

            );
            $this->Sarpra_model->create($dataStore);

            redirect('sarpra');
        
    }
    function edit($id)
    {
        $result = $this->Sarpra_model->read("id = '$id'");
        $data['sarpra'] = $result[0];
        $data['view'] = "sarpra/edit";
        $this->load->view('index', $data);
    }

    function update($id)
    {
        $dataStore = array(
            'kode_inventaris' => $this->input->post('kode_inventaris'),
            'nama_sarana_prasarana' => $this->input->post('nama_sarana_prasarana'),
            'usia_ekonomis' => $this->input->post('usia_ekonomis'),
            'status' => $this->input->post('status'),
            'stok' => $this->input->post('stok'),
            'tanggal_masuk' => $this->input->post('tanggal_masuk'),
            'keterangan' => $this->input->post('keterangan'),
        );

        $this->Sarpra_model->update("id = '$id'", $dataStore);
        return redirect('sarpra');
    }
}

?>