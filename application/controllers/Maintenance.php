<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Maintenance extends CI_COntroller
{
    function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');

        if(!$this->session->userdata("username")) redirect("login");

        $this->load->model('Sarpra_model');
        $this->load->model('Peminjaman_model');
        $this->load->model('Monitoring_model');
    }

    function index()
    {
        $data['result'] = $this->Sarpra_model->read("","id DESC");
        $data['view'] = "maintenance/index";
        $this->load->view('index', $data);
    }

    function create($id)
    {
        $data['sarpra'] = $this->Sarpra_model->read("id = '$id'")[0];
        $data['view'] = "maintenance/create";
        $this->load->view('index', $data);
    }

    function store($id)
    {   
        $sarpra = $this->Sarpra_model->read("id = '$id'")[0];

        if($sarpra->stok < $this->input->post('total_maintenance')) {
            echo "<script>javascript:alert('jumlah maintenance melebihi stok yang ada.'); window.location = '".site_url('maitenance/'.$id.'/create')."'</script>";
        } else {
            if($this->input->post('status') == 2 || $this->input->post('status') == 3) {
                $this->Sarpra_model->update("id = '$id'", array("stok" => $sarpra->stok - $this->input->post('total_maintenance')));
            } else {
                $this->Sarpra_model->update("id = '$id'", array("stok" => $sarpra->stok + $this->input->post('total_maintenance')));
            }

            $dataStore = array(
                'id_sarana_prasarana' => $id,
                'tanggal_maintenance' => $this->input->post('tanggal_maintenance'),
                'status' => $this->input->post('status'),
                'keterangan' => $this->input->post('keterangan'),
            );

            $this->Monitoring_model->create($dataStore);

            redirect('maintenance');
        }
    }
    
    function show($id)
    {
        $data['result'] = $this->Monitoring_model->read("id_sarana_prasarana = '$id'");
        $data['view'] = "maintenance/show";
        $this->load->view('index', $data);
    }

}

?>