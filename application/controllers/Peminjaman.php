<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Peminjaman extends CI_COntroller
{
    function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');

        if(!$this->session->userdata("username")) redirect("login");

        $this->load->model('Sarpra_model');
        $this->load->model('Peminjaman_model');
    }

    function index()
    {
        $data['result'] = $this->Sarpra_model->read();
        $data['view'] = "peminjaman/index";
        $this->load->view('index', $data);
    }

    function create($id)
    {
        $data['sarpra'] = $this->Sarpra_model->read("id = '$id'")[0];
        $data['view'] = "peminjaman/create";
        $this->load->view('index', $data);
    }

    function store($id)
    {
        $sarpra = $this->Sarpra_model->read("id = '$id'")[0];

        if($sarpra->stok < $this->input->post('jumlah')) {
            echo "<script>javascript:alert('jumlah melebihi stok yang ada.'); window.location = '".site_url('peminjaman/'.$id.'/create')."'</script>";
        } else {
            $this->Sarpra_model->update("id = '$id'", array("stok" => $sarpra->stok - $this->input->post('jumlah')));

            $dataStore = array(
                'id_user' => $this->session->userdata("id_user"),
                'id_sarana_prasarana' => $id,
                'tanggal_pengajuan' => $this->input->post('tanggal_pengajuan'),
                'tanggal_peminjaman' => $this->input->post('tanggal_peminjaman'),
                'tanggal_pengembalian' => $this->input->post('tanggal_pengembalian'),
                'jumlah' => $this->input->post('jumlah'),
                'keperluan_peminjaman' => $this->input->post('keperluan_peminjaman'),
            );
    
            $this->Peminjaman_model->create($dataStore);

            redirect('peminjaman/histori');
        }
    }
    function edit($id)
    {
        $result = $this->Sarpra_model->read("id = '$id'");
        $data['sarpra'] = $result[0];
        $data['view'] = "sarpra/edit";
        $this->load->view('index', $data);
    }

    function update($id)
    {
        $dataStore = array(
            'kode_inventaris' => $this->input->post('kode_inventaris'),
            'nama_sarana_prasarana' => $this->input->post('nama_sarana_prasarana'),
            'usia_ekonomis' => $this->input->post('usia_ekonomis'),
            'status' => $this->input->post('status'),
            'tanggal_masuk' => $this->input->post('tanggal_masuk'),
            'keterangan' => $this->input->post('keterangan'),
        );

        $this->Sarpra_model->update("id = '$id'", $dataStore);
        return redirect('sarpra');
    }

    function histori()
    {
        $idUser = $this->session->userdata("id_user");
        if($this->session->userdata("id_level") == 1) {
            $data['dataHistori'] = $this->Peminjaman_model->readJoinsarpra("","peminjaman.id DESC");
        } else {
            $data['dataHistori'] = $this->Peminjaman_model->readJoinsarpra("peminjaman.id_user = '$idUser'","peminjaman.id DESC");
        }
        $data['view'] = "peminjaman/histori";
        $this->load->view('index', $data);
    }

    function selesai($id_peminjaman) 
    {
        // get data peminjaman
        $dataPeminjaman = $this->Peminjaman_model->read("id = '$id_peminjaman'")[0];
        $datasarpra = $this->Sarpra_model->read("id = '$dataPeminjaman->id_sarana_prasarana'")[0];
        // update status pinjaman
        $this->Peminjaman_model->update("id = '$id_peminjaman'", array(
            'status' => 4
        ));

        $this->Sarpra_model->update("id = '$dataPeminjaman->id_sarana_prasarana'", array(
            'stok' => $dataPeminjaman->jumlah + $datasarpra->stok
        ));

        redirect("peminjaman/histori");
    }

    function tolak($id_peminjaman) 
    {
        $data['view'] = "sarpra/tolak";
        $data['id'] = $id_peminjaman;
        $this->load->view('index', $data);
    }

    function acc($id_peminjaman) 
    {
        // get data peminjaman
        $dataPeminjaman = $this->Peminjaman_model->read("id = '$id_peminjaman'")[0];
        $datasarpra = $this->Sarpra_model->read("id = '$dataPeminjaman->id_sarana_prasarana'")[0];
        // update status pinjaman
        $this->Peminjaman_model->update("id = '$id_peminjaman'", array(
            'status' => 2
        ));

        redirect("peminjaman/histori");
    }

    function storeTolak($id_peminjaman)
    {
           // get data peminjaman
           $dataPeminjaman = $this->Peminjaman_model->read("id = '$id_peminjaman'")[0];
           $datasarpra = $this->Sarpra_model->read("id = '$dataPeminjaman->id_sarana_prasarana'")[0];
           // update status pinjaman
           $this->Peminjaman_model->update("id = '$id_peminjaman'", array(
               'status' => 3,
               'keterangan' => $this->input->post('keterangan')
           ));
   
           $this->Sarpra_model->update("id = '$dataPeminjaman->id_sarana_prasarana'", array(
               'stok' => $dataPeminjaman->jumlah + $datasarpra->stok
           ));
   
           redirect("peminjaman/histori");
    }

    function show($id_peminjaman)
    {
        $data['result'] = $this->Peminjaman_model->readJoinsarpra("peminjaman.id = '$id_peminjaman'")[0];
        $data['view'] = "peminjaman/show";
        $this->load->view('index', $data);
    }

}

?>