-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 06, 2019 at 06:15 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yunan`
--

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`id`, `nama`) VALUES
(1, 'admin'),
(2, 'guru'),
(3, 'siswa'),
(4, 'tata usaha');

-- --------------------------------------------------------

--
-- Table structure for table `monitoring`
--

CREATE TABLE `monitoring` (
  `id` int(11) NOT NULL,
  `id_sarana_prasarana` int(11) NOT NULL,
  `tanggal_maintenance` date NOT NULL,
  `total_maintenance` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `peminjaman`
--

CREATE TABLE `peminjaman` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_sarana_prasarana` int(11) NOT NULL,
  `tanggal_pengajuan` date NOT NULL,
  `tanggal_peminjaman` date NOT NULL,
  `tanggal_pengembalian` date NOT NULL,
  `jumlah` int(11) NOT NULL,
  `keperluan_peminjaman` text NOT NULL,
  `keterangan` text,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `peminjaman`
--

INSERT INTO `peminjaman` (`id`, `id_user`, `id_sarana_prasarana`, `tanggal_pengajuan`, `tanggal_peminjaman`, `tanggal_pengembalian`, `jumlah`, `keperluan_peminjaman`, `keterangan`, `status`) VALUES
(3, 3, 1, '2019-09-10', '2019-09-16', '2019-09-16', 2, 'untuk bacaan', NULL, 4),
(4, 3, 2, '2019-09-10', '2019-09-18', '2019-09-23', 2, 'untuk belajar', NULL, 4),
(5, 3, 3, '2019-09-01', '2019-09-11', '2019-09-09', 2, 'test', NULL, 4),
(6, 3, 1, '2019-09-01', '2019-09-23', '2019-09-24', 2, 'test', NULL, 4),
(7, 3, 1, '2019-09-01', '2019-09-02', '2019-09-18', 8, 'test', NULL, 3),
(8, 3, 1, '2019-09-01', '2019-09-17', '2019-09-16', 10, 'test', NULL, 3),
(9, 3, 1, '2019-09-02', '2019-09-25', '2019-09-16', 12, 'test', 'harus bayar', 3),
(10, 3, 1, '2019-10-01', '2019-10-06', '2019-10-06', 2, 'test', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sarana_prasarana`
--

CREATE TABLE `sarana_prasarana` (
  `id` int(11) NOT NULL,
  `kode_inventaris` varchar(100) NOT NULL,
  `nama_sarana_prasarana` varchar(100) NOT NULL,
  `usia_ekonomis` int(11) NOT NULL,
  `stok` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `tanggal_masuk` date NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sarana_prasarana`
--

INSERT INTO `sarana_prasarana` (`id`, `kode_inventaris`, `nama_sarana_prasarana`, `usia_ekonomis`, `stok`, `status`, `tanggal_masuk`, `keterangan`) VALUES
(1, '001', 'laptop', 2, 8, 1, '2019-09-02', 'test'),
(2, '002', 'meja', 2, 10, 1, '2019-09-25', 'test'),
(3, '003', 'komputer', 2, 10, 1, '2019-09-09', 'test'),
(4, '004', 'buku', 2, 10, 1, '2019-09-02', 'test'),
(5, '005', 'kasur', 2, 10, 1, '2019-09-02', 'test'),
(6, '006', 'sepeda', 2, 10, 1, '2019-09-01', 'test'),
(7, '007', 'motor', 2, 10, 1, '2019-09-01', 'test'),
(8, '008', 'mobil', 2, 10, 1, '2019-09-04', 'test'),
(9, '010', 'tas', 2, 10, 1, '2019-09-18', 'test'),
(10, '011', 'buku sprint', 2, 10, 1, '2019-10-01', 'buku baru'),
(11, '012', 'alquran', 1, 20, 1, '2019-10-15', 'alquran'),
(12, '013', 'buku sejarah', 2, 10, 1, '2019-10-01', 'buku sejarah');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `id_level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nama_lengkap`, `username`, `password`, `id_level`) VALUES
(1, 'Admin Sekolah', 'admin', '123456', 1),
(2, 'Guru', 'guru', '123456', 2),
(3, 'Siswa', 'siswa', '123456', 3),
(4, 'Tata Usaha', 'tatausaha', '123456', 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `monitoring`
--
ALTER TABLE `monitoring`
  ADD PRIMARY KEY (`id`),
  ADD KEY `monitoring_id_sarana_prasarana_foreign` (`id_sarana_prasarana`);

--
-- Indexes for table `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD PRIMARY KEY (`id`),
  ADD KEY `peminjaman_id_sarana_prasarana_foreign` (`id_sarana_prasarana`),
  ADD KEY `peminjaman_id_user_foreign` (`id_user`);

--
-- Indexes for table `sarana_prasarana`
--
ALTER TABLE `sarana_prasarana`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id_level_foreign` (`id_level`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `monitoring`
--
ALTER TABLE `monitoring`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `peminjaman`
--
ALTER TABLE `peminjaman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `sarana_prasarana`
--
ALTER TABLE `sarana_prasarana`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `monitoring`
--
ALTER TABLE `monitoring`
  ADD CONSTRAINT `monitoring_id_sarana_prasarana_foreign` FOREIGN KEY (`id_sarana_prasarana`) REFERENCES `sarana_prasarana` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD CONSTRAINT `peminjaman_id_sarana_prasarana_foreign` FOREIGN KEY (`id_sarana_prasarana`) REFERENCES `sarana_prasarana` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `peminjaman_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_id_level_foreign` FOREIGN KEY (`id_level`) REFERENCES `level` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
